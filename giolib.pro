TEMPLATE = app
CONFIG += c++1z
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -g

DEFINES += GIO_WRAP_THROW

linux {
    QMAKE_LIBS += -ldw -pthread
}

!win32 {
    QMAKE_LIBS += -lboost_unit_test_framework -lboost_system -lboost_filesystem
}

macx {
    QMAKE_CXXFLAGS += -I/usr/local/include
    QMAKE_LIBS += -L/usr/local/lib
}

win32 {
    DEFINES += BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE
    DEFINES += NOMINMAX
    #DEFINES += BOOST_LIB_DIAGNOSTIC
    INCLUDEPATH += c:\local\boost c:\libs
    QMAKE_CXXFLAGS += /wd4250 /wd4200 /Zi /std:c++17
    QMAKE_LFLAGS += /STACK:8388608 /DEBUG:FULL
    QMAKE_LIBS += /LIBPATH:c:\local\boost\lib64-msvc-14.2 /LIBPATH:c:\libs
    CONFIG += console
}

SOURCES += \
    encoding.cpp \
    giolib.cpp \
    platform_utils.cpp \
    std_printers.cpp \
    include_all.cpp \
    platform.cpp \
    stacktrace.cpp \
    memory.cpp \
    base64.cpp \
    locker.cpp \
    bitmask.cpp \
    proc_stats.cpp \
    assert.cpp \
    containers.cpp

HEADERS += \
    giolib/assert.h \
    giolib/encoding.h \
    giolib/utils.h \
    giolib/static_block.h \
    giolib/main.h \
    giolib/std_printers.h \
    giolib/detail/std_printers.h \
    test.h \
    giolib/containers.h \
    giolib/memory.h \
    giolib/stacktrace.h \
    giolib/platform.h \
    giolib/exception.h \
    giolib/crash.h \
    giolib/base64.h \
    giolib/locker.h \
    giolib/bitmask.h \
    giolib/proc_stats.h \
    giolib/platform_utils.h \
    giolib/inheritance.h

DISTFILES += \
    README.md
