/* This file is part of giolib, a collection of misc C++ code
   Copyright (C) 2019 Giovanni Mascellani <gio@debian.org>
   https://gitlab.com/giomasce/giolib

   This program is licensed under the terms of the Boost
   Software License 1.0.

   Boost Software License - Version 1.0 - August 17th, 2003

   Permission is hereby granted, free of charge, to any person or organization
   obtaining a copy of the software and accompanying documentation covered by
   this license (the "Software") to use, reproduce, display, distribute,
   execute, and transmit the Software, and to prepare derivative works of the
   Software, and to permit third-parties to whom the Software is furnished to
   do so, all subject to the following:

   The copyright notices in the Software and this entire statement, including
   the above license grant, this restriction and the following disclaimer,
   must be included in all copies of the Software, in whole or in part, and
   all derivative works of the Software, unless such copies or derivative
   works are solely in the form of machine-executable object code generated by
   a source language processor.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
   SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
   FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE. */

#define GIO_STD_PRINTERS_INJECT_IN_ROOT
#include <giolib/std_printers.h>
#include <giolib/assert.h>
#include <giolib/main.h>

GIO_MAIN_NOARGS(assert) {
    gio_force_assert(false);
}

GIO_MAIN_NOARGS(assert_ctx) {
    gio_force_assert_ctx(false, "just a random assertion");
}

GIO_MAIN_NOARGS(assert_ctx2) {
    gio_force_assert_ctx(false, "just a random assertion with data: " << 42);
}

GIO_MAIN_NOARGS(assert_ctx3) {
    gio_force_assert_ctx(false, std::make_tuple("just a random assertion with data: ",  42));
}

#undef NDEBUG
#include <cassert>

GIO_MAIN_NOARGS(assert_libc) {
    assert(false);
    // MSVC cannot infer that this is unreachable and wants a return statement
    return 0;
}
