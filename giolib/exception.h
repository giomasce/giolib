/* This file is part of giolib, a collection of misc C++ code
   Copyright (C) 2018 Giovanni Mascellani <gio@debian.org>
   https://gitlab.com/giomasce/giolib

   This program is licensed under the terms of the Boost
   Software License 1.0.

   Boost Software License - Version 1.0 - August 17th, 2003

   Permission is hereby granted, free of charge, to any person or organization
   obtaining a copy of the software and accompanying documentation covered by
   this license (the "Software") to use, reproduce, display, distribute,
   execute, and transmit the Software, and to prepare derivative works of the
   Software, and to permit third-parties to whom the Software is furnished to
   do so, all subject to the following:

   The copyright notices in the Software and this entire statement, including
   the above license grant, this restriction and the following disclaimer,
   must be included in all copies of the Software, in whole or in part, and
   all derivative works of the Software, unless such copies or derivative
   works are solely in the form of machine-executable object code generated by
   a source language processor.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
   SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
   FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE. */

#pragma once

#include <iostream>
#include <csignal>
#include <utility>

#include <boost/type_index.hpp>
#include <boost/current_function.hpp>

#include "platform.h"
#include "stacktrace.h"
#include "crash.h"
#include "utils.h"

#if defined(GIO_PLATFORM_LINUX) || defined(GIO_PLATFORM_MACOS)

#include <cxxabi.h>

#include <string>
#include <typeinfo>

namespace gio {

inline std::string current_exception_type() {
    auto exc_type = abi::__cxa_current_exception_type();
    if (!exc_type) {
        return "";
    }
    int status;
    auto demangled = abi::__cxa_demangle(exc_type->name(), nullptr, nullptr, &status);
    if (status != 0 || !demangled) {
        free(demangled);
        return "";
    }
    std::string ret(demangled);
    free(demangled);
    return ret;
}

}

#endif

#if defined(GIO_PLATFORM_WIN32) || defined(GIO_PLATFORM_UNKNOWN)

namespace gio {

inline std::string current_exception_type() {
    return "";
}

}

#endif

namespace gio {

namespace detail {

typedef stacktrace stacktrace_t;

class exception_wrapper_base {
public:
    exception_wrapper_base(std::string file, size_t line, std::string func)
        : file_(std::move(file)), line_(line), func_(std::move(func)), stacktrace_(get_stacktrace()) {
    }

    virtual ~exception_wrapper_base() = default;

    virtual std::string wrapped_type_name() const = 0;

    const std::string &file() const noexcept {
        return this->file_;
    }

    const std::string &func() const noexcept {
        return this->func_;
    }

    size_t line() const noexcept {
        return this->line_;
    }

    const detail::stacktrace_t &stacktrace() const noexcept {
        return this->stacktrace_;
    }

private:
    std::string file_;
    size_t line_;
    std::string func_;
    detail::stacktrace_t stacktrace_;
};

template<typename T>
class generic_exception_wrapper : public exception_wrapper_base, public T {
public:
    template<typename... Args>
    generic_exception_wrapper(std::string file, size_t line, std::string func, Args &&... args)
        : exception_wrapper_base(std::move(file), line, std::move(func)),
          T(std::forward<Args>(args)...) {
    }

    virtual std::string wrapped_type_name() const override {
        return boost::typeindex::type_id<T>().pretty_name();
    }
};

class std_exception_wrapper_base : public exception_wrapper_base {
    using exception_wrapper_base::exception_wrapper_base;
public:
    virtual const char *orig_what() const noexcept = 0;
};

template<typename T>
class std_exception_wrapper : public std_exception_wrapper_base, public T {
public:
    template<typename... Args>
    std_exception_wrapper(std::string file, size_t line, std::string func, Args &&... args)
        : std_exception_wrapper_base(std::move(file), line, std::move(func)),
          T(std::forward<Args>(args)...) {
        this->what_ = gio_make_string(this->orig_what() << " at " << this->file() << ':' << this->line() << " in " << this->func());
    }

    const char *what() const noexcept override {
        //dump_stacktrace(std::cerr, this->st);
        return this->what_.c_str();
    }

    const char *orig_what() const noexcept override {
        return this->T::what();
    }

    std::string wrapped_type_name() const override {
        return boost::typeindex::type_id<T>().pretty_name();
    }

private:
    std::string what_;
};

template<typename T, typename... Args>
[[noreturn]]
void throw_exception_wrapper(std::string file, size_t line, std::string func, Args &&... args) {
    if constexpr (!std::is_class_v<T> || std::is_final_v<T>) {
        // Not a class type, we cannot extend it with the wrapper
        (void) file;
        (void) line;
        (void) func;
        throw T(std::forward<Args>(args)...);
    } else if constexpr (std::is_base_of_v<std::exception, T>) {
        throw std_exception_wrapper<T>(std::move(file), line, std::move(func), std::forward<Args>(args)...);
    } else {
        throw generic_exception_wrapper<T>(std::move(file), line, std::move(func), std::forward<Args>(args)...);
    }
}

}

inline void default_exception_handler(std::exception_ptr ptr, std::ostream &os = std::cerr) {
    try {
        if (ptr) {
            std::rethrow_exception(ptr);
        }
    } catch (const detail::std_exception_wrapper_base &e) {
        os << "Wrapped exception of type " << e.wrapped_type_name() << ": " << e.orig_what() << '\n';
        os << "Exception happened in function " << e.func() << '\n';
        os << "In file " << e.file() << ':' << e.line() << std::endl;
    } catch (const detail::exception_wrapper_base &e) {
        os << "Wrapped exception of type " << e.wrapped_type_name() << '\n';
        os << "Exception happened in function " << e.func() << '\n';
        os << "In file " << e.file() << ':' << e.line() << std::endl;
    } catch (const std::exception &e) {
        os << "Exception of dynamic type " << boost::typeindex::type_id_runtime(e).pretty_name() << ": " << e.what() << std::endl;
    } catch (const char* &e) {
        os << "Exception of type char*: " << e << std::endl;
    } catch (const std::string &e) {
        os << "Exception of type std::string: " << e << std::endl;
    } catch (...) {
        std::string exc_type = current_exception_type();
        if (exc_type == "") {
            exc_type = "(unknown)";
        }
        os << "Exception of unmanaged type " << exc_type << std::endl;
    }
}

[[noreturn]] inline void default_terminator() {
    std::cerr << "Program unexpectedly terminated\n";
    dump_stacktrace(std::cerr, get_stacktrace());
    default_exception_handler(std::current_exception());
    safe_abort();
}

inline void install_exception_handler() {
    std::set_terminate(default_terminator);
}

}

#define gio_throw_unwrapped(klass, ...) throw klass(__VA_ARGS__)
#define gio_throw_wrapped(klass, ...) gio::detail::throw_exception_wrapper<klass>(__FILE__, __LINE__, BOOST_CURRENT_FUNCTION, ##__VA_ARGS__)
#ifdef GIO_WRAP_THROW
#define gio_throw(klass, ...) gio_throw_wrapped(klass, ##__VA_ARGS__)
#else
#define gio_throw(klass, ...) gio_throw_unwrapped(klass, ##__VA_ARGS__)
#endif
